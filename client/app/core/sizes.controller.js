angular.module('artoo').controller('SizesCtrl', function($scope, SizesSrv) {

  //user values  
  $scope.user = {
    shoulders: 65,
    hips: 57,
    waist: 65,
  };

  //partner values   
  $scope.partner = {
    shoulders: {
      xs: {min:50, max:60},
      s: {min:60, max:70} 
    },
    hips: {
      xs: {min:50, max:60},
      s: {min:60, max:70}
    },
    waist: {
      xs:{min:50, max:60},
      s: {min:60, max:70}
    }
  };

  // Init filterProp
  $scope.SizesSrv = SizesSrv.filterProp($scope.user, $scope.partner);
});